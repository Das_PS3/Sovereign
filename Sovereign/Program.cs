﻿using System;
using System.IO;
using System.Windows.Forms;

namespace Sovereign
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 1)
                if (args[0].ToLower() == "-version")
                {
                    Console.WriteLine(String.Format("{0}, written by {1}", Application.ProductName, Application.CompanyName));

                    Environment.Exit(0);
                }

            DirectoryInfo securityCheck = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory);
            if (securityCheck.Name != "Harvest")
            {
                Console.WriteLine("Error. For security, the parent folder's name must be \"Harvest\".");
                Console.ReadKey();
                Environment.Exit(0);
            }

            string[] extensions = { "*.backup", "*.edat", "*.self", "*.sdat", "*.sprx" };

            foreach (string folder in Directory.GetDirectories(AppDomain.CurrentDomain.BaseDirectory))
                if (new DirectoryInfo(folder).Name.Length == 9)
                {
                    File.SetAttributes(folder + @"\ICON0.PNG", FileAttributes.ReadOnly);
                    File.SetAttributes(folder + @"\PARAM.SFO", FileAttributes.ReadOnly);
                    if (File.Exists(folder + @"\USRDIR\EBOOT.BIN"))
                        File.SetAttributes(folder + @"\USRDIR\EBOOT.BIN", FileAttributes.ReadOnly);

                    for (int i = 0; i < extensions.Length; i++)
                        foreach (string file in Directory.GetFiles(folder, extensions[i], SearchOption.AllDirectories))
                            File.SetAttributes(file, FileAttributes.ReadOnly);
                    
                    foreach (string file in Directory.GetFiles(folder, "*", SearchOption.AllDirectories))
                        try 
                        {
                            File.Delete(file);
                        }
                        catch (UnauthorizedAccessException) {}
                   
                    Misc_Utils.DeleteEmptyDirs(folder);

                    foreach (string file in Directory.GetFiles(folder, "*", SearchOption.AllDirectories))
                        File.SetAttributes(file, FileAttributes.Normal);
                }
        }
    }
}
